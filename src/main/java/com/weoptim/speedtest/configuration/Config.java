package com.weoptim.speedtest.configuration;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Config {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        RestTemplate rt =  new RestTemplate();
        rt.setRequestFactory( new HttpComponentsClientHttpRequestFactory() );
        return rt;
    }


}
