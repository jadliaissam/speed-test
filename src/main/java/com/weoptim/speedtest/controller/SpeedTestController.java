package com.weoptim.speedtest.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class SpeedTestController {

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/time/{serieName}/{start}/{end}",method = RequestMethod.GET)
    @ApiOperation(value = "Try searching {quantity} time series and return the elapsed time.")
    public String testTime(@PathVariable String start, @PathVariable String end, @PathVariable String serieName){
        long deb = System.currentTimeMillis();
        int start_id = Integer.valueOf(start);
        int end_id = Integer.valueOf(end);
        List<String> ids = new ArrayList<String>();
        for(int i=start_id; i<end_id; i++) ids.add(i+"");
        Map series = restTemplate.postForObject("http://seriesMS/serie/"+serieName+"/list",ids,Map.class);
        long fin = System.currentTimeMillis();
        return "Taille : "+series.size()+" . Execution Time : "+(fin-deb);
    }

    @RequestMapping(value = "/data/{serieName}/{start}/{end}",method = RequestMethod.GET)
    @ApiOperation(value = "Try searching {quantity} time series and return the time series found")
    public Map getData(@PathVariable String start, @PathVariable String end, @PathVariable String serieName){
        int start_id = Integer.valueOf(start);
        int end_id = Integer.valueOf(end);
        List<String> ids = new ArrayList<String>();
        for(int i=start_id; i<end_id; i++) ids.add(i+"");
        Map series = restTemplate.postForObject("http://seriesMS/serie/"+serieName+"/list",ids,Map.class);
        return series;
    }


}
